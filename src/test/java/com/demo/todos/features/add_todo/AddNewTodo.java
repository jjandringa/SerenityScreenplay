/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.todos.features.add_todo;

import com.demo.todos.questions.TodoItemsList;
import com.demo.todos.tasks.AddATodoItem;
import com.demo.todos.tasks.StartWith;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.CoreMatchers.hasItem;

/**
 *
 * @author handringa
 */
@RunWith(SerenityRunner.class)
public class AddNewTodo {

    Actor hans = Actor.named("Hans");

    @Managed(driver = "chrome")
    WebDriver theBrowser;

    @Before
    public void canBrowseTheWeb() {
        // de givenThat kan ook weg worden gelaten, vooral wanneer cucumber gebruikt wordt is het niet nodig
        givenThat(hans).can(BrowseTheWeb.with(theBrowser));
    }

    @Test
    public void should_be_able_to_add_the_first_todo_item() {
        givenThat(hans).wasAbleTo(StartWith.anEmptyTodoList());
        when(hans).attemptsTo(AddATodoItem.called("Prepare for a DEMO"));
        then(hans).should(seeThat(TodoItemsList.displayed(), hasItem("Prepare for a DEMO")));
    }
}
