/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.todos.tasks;

import com.demo.todos.ui.TodoList;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import static net.serenitybdd.screenplay.Tasks.instrumented;
import net.serenitybdd.screenplay.actions.Enter;
import org.openqa.selenium.Keys;

/**
 *
 * @author handringa
 */
public class AddATodoItem implements Task {

    private String item;

    public AddATodoItem(String item) {
        this.item = item;
    }

    @Override
//    @Step("{0} adds an item called '#item'")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(item).into(TodoList.TODO_FIELD).thenHit(Keys.RETURN)
        );
    }

    public static AddATodoItem called(String item) {
        return instrumented(AddATodoItem.class, item);
    }
    
}
