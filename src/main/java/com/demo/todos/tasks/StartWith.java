/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.todos.tasks;

import com.demo.todos.ui.ApplicationHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import net.thucydides.core.annotations.Step;

/**
 *
 * @author handringa
 */
public class StartWith implements Task {

    ApplicationHomePage applicationHomePage;

    @Override
    @Step("{0} starts with an empty todo list")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.browserOn().the(applicationHomePage)
        );
    }

    public static StartWith anEmptyTodoList() {
        return instrumented(StartWith.class);
    }

}
