/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.todos.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static com.demo.todos.ui.TodoList.LIST_ITEMS;

/**
 *
 * @author handringa
 */
public class TodoItemsList implements Question<List<String>> {

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(LIST_ITEMS)
                .viewedBy(actor)
                .asList();
    }

    public static Question<List<String>> displayed() {
        return new TodoItemsList();
    }
}
