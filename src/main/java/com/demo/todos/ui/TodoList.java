/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demo.todos.ui;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

/**
 *
 * @author handringa
 */
public class TodoList extends PageObject {
    public static Target TODO_FIELD = Target
            .the("'What needs to be done?' field")
            .locatedBy(".new-todo");
 
    public static Target LIST_ITEMS = Target
            .the("List of todo items")
            .locatedBy(".view label");
}
